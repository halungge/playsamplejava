package controllers;

/**
 * imports 
 * play.mvc.* -> JAVA
 * play.api.mvc.* -> SCALA
 */

import models.Task;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;

public class Application extends Controller {

	static Form<Task> taskForm = form(Task.class);

	public static Result index() {
		return redirect(routes.Application.tasks());

	}

	/*
	 * Templates are compiled in to Scala functions. For Java a class
	 * is created in in views.html.xxx. with a render(...) function
	 */
	public static Result tasks() {
		return ok(views.html.index.render(Task.all(), taskForm));
	}

	public static Result newTask() {
		Form<Task> filledForm = taskForm.bindFromRequest();
		if (filledForm.hasErrors()) {
			return badRequest(views.html.index.render(Task.all(), filledForm));
		} else {
			Task.create(filledForm.get());
			return redirect(routes.Application.tasks());
		}
	}

	public static Result deleteTask(Long id) {
		Task.delete(id);
		return redirect(routes.Application.tasks());
	}

}