package models;

import java.util.*;
import play.db.ebean.*;
import javax.persistence.*;
import play.data.validation.Constraints.*;


/*
 * using ebean ORM which is included in play
 * http://www.avaje.org/ebean/documentation.html
 * 
 * Play does generate getters/setter automatically (reason compatiblity).
 * BUT: 1) generation is done after compilation, you cannot use them 
 *      directly! -> fields have to be public...
 *      2) in the views you have to access the fields directly, much alike Scala
 *      generation of getters/setter can generate inconsistencies with eclipse
 *      (eclipse -> configure Build Path> set output folder to a folder not used by SBT 
 *      fx project_base/tmp   
 *      2) if you provide them, yours are used
 */
@Entity
public class Task extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Required
	private String label;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public static Finder<Long, Task> find = new Finder<Long, Task>(Long.class,
			Task.class);

	public static List<Task> all() {
		return find.all();
	}

	public static void create(Task task) {
		task.save();
	}

	public static void delete(Long id) {
		find.ref(id).delete();
	}

}